﻿namespace TeleAnalyzer {
    using System;
    using System.Globalization;
    using System.Windows.Data;

    internal class CircleFontConstrainer : IMultiValueConverter {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
            var first = (double) values[0] * (50 / 450d);
            var second = (double) values[1] * (50 / 800d);
            return first < second ? first : second;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) {
            return null;
        }
    }
}