﻿namespace TeleAnalyzer {
    internal class AiringBlockDummy {
        public string Start { get; set; }
        public string End { get; set; }
        public string Duration { get; set; }
    }
}