﻿namespace TeleAnalyzer {
    using System;
    using System.Runtime.Serialization;

    public class LinkedListQueue<T> {
        volatile LinkedListNode<T> _headRef;
        volatile LinkedListNode<T> _tailRef;

        public LinkedListQueue() {
            this.Count = 0;
        }

        public uint Count { get; private set; }

        public void Enqueue(T item) {
            var newNode = new LinkedListNode<T> {Element = item};
            if (this._tailRef != null) this._tailRef.NextRef = newNode;
            this._tailRef = newNode;
            if (this._headRef == null) this._headRef = newNode;
            this.Count++;
        }

        public T Dequeue() {
            if (this._headRef == null) throw new EmptyQueueException("Attempted Dequeue on empty list.");
            var currentItem = this._headRef.Element;
            this._headRef = this._headRef.NextRef;
            this.Count--;
            return currentItem;
        }

        //public void Clear() {
        //    _headRef = null;
        //    _tailRef = null;
        //    Count = 0;
        //    GC.Collect();
        //}

        public bool IsEmpty() {
            return this.Count <= 0;
        }

        //inner class
        class LinkedListNode<TK> {
            public TK Element { get; set; }
            public LinkedListNode<TK> NextRef { get; set; }
        }

        class EmptyQueueException : InvalidOperationException {
            public EmptyQueueException() { }
            public EmptyQueueException(string message) : base(message) { }
            public EmptyQueueException(string message, Exception innerException) : base(message, innerException) { }
            protected EmptyQueueException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        }
    }
}