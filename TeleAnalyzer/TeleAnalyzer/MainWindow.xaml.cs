﻿namespace TeleAnalyzer {
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using Xceed.Wpf.Toolkit.Primitives;

    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        readonly string Conglomerate;
        readonly List<string[]> Networks;
        readonly Dictionary<int, List<Episode>> Seasons;
        ConglomeratePage CPg;
        bool ProgramReset;
        List<string[]> Programs;

        public MainWindow(ConglomeratePage CPg, string conglomerate, string CId) {
            this.Seasons = new Dictionary<int, List<Episode>>();
            this.Networks = App.DbC.NetworksByConglomerate(CId);
            this.Conglomerate = conglomerate;

            this.InitializeComponent();

            foreach (string[] tuple in this.Networks) this.NetworkComboBox.Items.Add(tuple[0]);
            this.NetworkBlocker.Opacity = 0;
            this.NetworkBlocker.IsHitTestVisible = false;
        }

        void ProgramComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (this.ProgramComboBox.SelectedIndex < 0) return;
            this.ProgramReset = true;
            this.SeasonRoot.Items.Clear();
            this.PerfRefSet.Items.Clear();
            this.InsgtSet.Items.Clear();
            this.Viewers.Content = this.Rating.Content = "-";
            this.RatingsBlocker.Opacity = .6;
            foreach (int[] tuple in App.DbC.GetSeasonAndEpisode(this.Programs[this.ProgramComboBox.SelectedIndex][1], this.Networks[this.NetworkComboBox.SelectedIndex][1]))
                if (this.Seasons.TryGetValue(tuple[0], out List<Episode> episodes))
                    episodes.Add(new Episode {EpN = tuple[1], EId = tuple[2]});
                else
                    this.Seasons.Add(tuple[0], new List<Episode> {new Episode {EpN = tuple[1], EId = tuple[2]}});

            EpisodeTab first = null;
            var epN = "1";
            var snN = "1";
            foreach (KeyValuePair<int, List<Episode>> entry in this.Seasons) {
                // do something with entry.Value or entry.Key
                //Debug.WriteLine($"{entry.Value},{entry.Key}");

                var episodeRoot = new TabControl {
                    TabStripPlacement = Dock.Right,
                    Height = double.NaN
                };
                episodeRoot.SetBinding(FontSizeProperty,
                    new MultiBinding {
                        Bindings = {
                            new Binding("ActualHeight") {
                                ElementName = "Window",
                                Mode = BindingMode.OneWay
                            },
                            new Binding("ActualWidth") {
                                ElementName = "Window",
                                Mode = BindingMode.OneWay
                            }
                        },
                        Converter = new FontRatioConverter()
                    });
                var sV = new ScrollViewer {
                    FlowDirection = FlowDirection.RightToLeft,
                    VerticalAlignment = VerticalAlignment.Top
                };
                foreach (var episode in entry.Value) {
                    var epT = new EpisodeTab();
                    var ep = new TabItem {
                        Header = $"Ep {episode.EpN}",
                        FlowDirection = FlowDirection.LeftToRight,
                        VerticalContentAlignment = VerticalAlignment.Top,
                        Content = epT
                    };
                    epT.SetBinding(HeightProperty, new Binding("ActualHeight") {
                        ElementName = "SeasonRoot",
                        Mode = BindingMode.OneWay
                    });
                    ep.PreviewMouseUp += (s, o) => {
                        //MessageBox.Show($"{entry.Key},{episode.EpN}");
                        string[] epData = App.DbC.GetEpisodeData(entry.Key.ToString(), episode.EpN.ToString(), this.Programs[this.ProgramComboBox.SelectedIndex][1]);
                        if (epData != null) {
                            epT.EpName.Content = epData[0];
                            epT.Premier.Content = epData[1];
                            epT.Duration.Content = $"{epData[2]} hour(s)";
                            epT.Loading = false;
                        }
                        string[] rtData = App.DbC.GetRatingsData(entry.Key.ToString(), episode.EpN.ToString(), this.Programs[this.ProgramComboBox.SelectedIndex][1]);
                        if (rtData == null) return;
                        this.Viewers.Content = $"{rtData[0]} M";
                        this.Rating.Content = $"{rtData[1]}";
                        this.RatingsBlocker.Opacity = 0;
                        this.scrollViewer.ScrollToHome();
                        sV.ScrollToHome();
                        sV.UpdateLayout();
                    };
                    episodeRoot.Items.Add(ep);

                    this.PerfRefSet.Items.Add($"S{entry.Key}E{episode.EpN}");
                    this.InsgtSet.Items.Add($"S{entry.Key}E{episode.EpN}");
                    this.PerfRefSet.SelectedItems.Add($"S{entry.Key}E{episode.EpN}");
                    this.InsgtSet.SelectedItems.Add($"S{entry.Key}E{episode.EpN}");

                    if (first == null) {
                        first = epT;
                        epN = episode.EpN.ToString();
                        snN = entry.Key.ToString();
                    }
                }
                sV.SetBinding(HeightProperty,
                    new Binding("ActualHeight") {
                        ElementName = "scrollViewer",
                        Mode = BindingMode.OneWay
                    });
                sV.Content = episodeRoot;
                var sTb = new TabItem {
                    Header = $"Season {entry.Key}",
                    Content = sV
                    //VerticalContentAlignment = VerticalAlignment.Top
                };
                this.SeasonRoot.Items.Add(sTb);
            }

            if (first == null) return;

            string[] localEpData = App.DbC.GetEpisodeData(snN, epN, this.Programs[this.ProgramComboBox.SelectedIndex][1]);
            if (localEpData != null) {
                first.EpName.Content = localEpData[0];
                first.Premier.Content = localEpData[1];
                first.Duration.Content = $"{localEpData[2]} hour(s)";
                first.Loading = false;
            }
            string[] localRtData = App.DbC.GetRatingsData(snN, epN, this.Programs[this.ProgramComboBox.SelectedIndex][1]);
            if (localRtData == null) return;
            this.Viewers.Content = $"{localRtData[0]} M";
            this.Rating.Content = $"{localRtData[1]}";
            this.RatingsBlocker.Opacity = 0;
            this.scrollViewer.ScrollToHome();
            this.ProgramReset = false;
            this.PerfRefSet_ItemSelectionChanged(this, null);
            this.InsgtSet_ItemSelectionChanged(this, null);
        }

        void NetworkComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (this.NetworkComboBox.SelectedIndex < 0) return;
            string[] networkData = App.DbC.GetNetworkData(this.Networks[this.NetworkComboBox.SelectedIndex][1]);
            this.ConglomerateLabel.Content = this.Conglomerate;
            this.NetworkLabel.Content = networkData[0];
            this.TypeLabel.Content = networkData[1];
            this.AvgEpLabel.Content = $"{App.DbC.NetworkRatingAverage(this.Networks[this.NetworkComboBox.SelectedIndex][1]):0.00000}";
            this.PortfolioLabel.Content = $"{App.DbC.PortfolioCount(this.Networks[this.NetworkComboBox.SelectedIndex][1])} Program(s)";
            this.ProfileBlocker.Opacity = 0;
            this.ProfileBlocker.IsHitTestVisible = false;

            this.SeasonRoot.Items.Clear();
            this.Viewers.Content = this.Rating.Content = "-";
            this.RatingsBlocker.Opacity = .6;

            this.Programs = App.DbC.GetPrograms(this.Networks[this.NetworkComboBox.SelectedIndex][1]);
            this.ProgramComboBox.Items.Clear();
            foreach (string[] tuple in this.Programs) this.ProgramComboBox.Items.Add(tuple[0]);
            this.ProgramBlocker.Opacity = 0;
            this.ProgramBlocker.IsHitTestVisible = false;
        }

        void SeasonRoot_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (this.ProgramReset) return;
            this.Viewers.Content = this.Rating.Content = "-";
            this.RatingsBlocker.Opacity = .6;

            var sV = (ScrollViewer) this.SeasonRoot.SelectedContent;
            var episodeRoot = (TabControl) sV.Content;
            if (episodeRoot.SelectedIndex < 0) episodeRoot.SelectedIndex = 0;

            var season = new string(((TabItem) this.SeasonRoot.Items[this.SeasonRoot.SelectedIndex]).Header.ToString().Where(char.IsDigit).ToArray());
            var episode = new string(((TabItem) episodeRoot.Items[episodeRoot.SelectedIndex < 0 ? 0 : episodeRoot.SelectedIndex]).Header.ToString().Where(char.IsDigit).ToArray());

            //Debug.WriteLine($"\n\n\n{season},{episode}\n\n\n");

            var epT = (EpisodeTab) episodeRoot.SelectedContent;
            if (epT != null) {
                string[] epData = App.DbC.GetEpisodeData(season, episode, this.Programs[this.ProgramComboBox.SelectedIndex][1]);
                if (epData != null) {
                    epT.EpName.Content = epData[0];
                    epT.Premier.Content = epData[1];
                    epT.Duration.Content = $"{epData[2]} hour(s)";
                    epT.Loading = false;
                }
            }
            //Debug.WriteLine($"\n\n{season},{episode}\n\n");
            string[] rtData = App.DbC.GetRatingsData(season, episode, this.Programs[this.ProgramComboBox.SelectedIndex][1]);
            if (rtData == null) return;
            this.Viewers.Content = $"{rtData[0]} M";
            this.Rating.Content = $"{rtData[1]}";
            this.RatingsBlocker.Opacity = 0;
            this.scrollViewer.ScrollToHome();
        }

        void PerfRefSet_ItemSelectionChanged(object sender, ItemSelectionChangedEventArgs e) {
            this.BarChart.Plot(new double[] {0});
            if (this.ProgramReset) return;
            if (string.IsNullOrEmpty(this.PerfRefSet.SelectedValue) || this.PerfRefSet.Items.Count <= 0) return;
            string[] tuples = this.PerfRefSet.SelectedValue.Split(',');
            var eIds = new List<string>();
            Debug.WriteLine($"\n\n\n\n\n{tuples.Length}");
            foreach (var t in tuples) {
                string[] tmp = t.Substring(1).Split('E');
                //Debug.WriteLine($"\n\n\n\n{i}:{tmp[0]},{tmp[1]}");
                //seasons[i] = tmp[0];
                //episodes[i] = tmp[1];
                if (!int.TryParse(tmp[0], out int season)) return;
                if (!int.TryParse(tmp[1], out int episode)) return;

                if (!this.Seasons.TryGetValue(season, out List<Episode> ep)) return;
                eIds.AddRange(from s in ep
                    where s.EpN == episode
                    select s.EId.ToString());
            }

            List<float[]> hist = App.DbC.RatingsHistogram(eIds.ToArray());

            var N = hist.Count;
            var y = new double[N];
            for (var i = 0; i < N; i++) y[i] = hist[i][1];
            this.BarChart.PlotBars(y);

            this.DistributionBlocker.Opacity = 0;
            this.ComparisonBlocker.Opacity = 0;
            this.Mean.Content = $"{App.DbC.Mean(eIds.ToArray()):0.000} M";
            this.StdDev.Content = $"{App.DbC.StdDev(eIds.ToArray()):0.000} M";
        }

        void InsgtSet_ItemSelectionChanged(object sender, ItemSelectionChangedEventArgs e) {
            this.InsightChart.Plot(new double[] {0}, new double[] {0});
            if (this.ProgramReset) return;
            if (string.IsNullOrEmpty(this.InsgtSet.SelectedValue) || this.InsgtSet.Items.Count <= 0) return;
            string[] tuples = this.InsgtSet.SelectedValue.Split(',');
            var eIds = new List<string>();
            Debug.WriteLine($"\n\n\n\n\n{tuples.Length}");
            foreach (var t in tuples) {
                string[] tmp = t.Substring(1).Split('E');
                //Debug.WriteLine($"\n\n\n\n{i}:{tmp[0]},{tmp[1]}");
                //seasons[i] = tmp[0];
                //episodes[i] = tmp[1];
                if (!int.TryParse(tmp[0], out int season)) return;
                if (!int.TryParse(tmp[1], out int episode)) return;

                if (!this.Seasons.TryGetValue(season, out List<Episode> ep)) return;
                eIds.AddRange(from s in ep
                    where s.EpN == episode
                    select s.EId.ToString());
            }

            List<float> data = App.DbC.RatingsGraph(eIds.ToArray());
            var N = data.Count;
            var x = new double[N];
            var y = new double[N];
            for (var i = 0; i < N; i++) {
                x[i] = i;
                y[i] = data[i];
            }
            var sx = x.StdDev();
            var sy = y.StdDev();
            var mx = x.Average();
            var my = y.Average();

            var rxy = 1 / (N - 1.0);
            var stdSc = 0.0;
            for (var i = 0; i < N; i++) stdSc += (x[i] - mx) / sx * ((y[i] - my) / sy);
            rxy *= stdSc;

            var b1 = rxy * (sy / sx);
            var b0 = my - b1 * mx;

            this.PredictedViewers.Content = $"{b0 + b1 * N + 1:0.000} M";
            this.B0.Content = $@"β0: {b0:0.000}";
            this.B1.Content = $@"β1: {b1:0.000}";

            this.Correlation.Content = $"{rxy * rxy:0.000}";

            this.InsightChart.Plot(x, y);
            this.PredictionChartBlocker.Opacity = 0;
            this.InsightBlocker.Opacity = 0;
        }

        void Button_PreviewMouseUp(object sender, MouseButtonEventArgs e) {
            if (this.CPg == null) this.CPg = new ConglomeratePage();
            this.CPg.Show();
            this.Close();
        }

        struct Episode {
            public int EpN;
            public int EId;
        }
    }
}