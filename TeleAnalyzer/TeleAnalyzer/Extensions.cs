﻿namespace TeleAnalyzer {
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class Extensions {
        public static double StdDev(this IEnumerable<double> values) {
            double ret = 0;
            var count = values.Count();
            if (count <= 1) return ret;
            //Compute the Average
            var avg = values.Average();

            //Perform the Sum of (value-avg)^2
            var sum = values.Sum(d => (d - avg) * (d - avg));

            //Put it all together
            ret = Math.Sqrt(sum / count);
            return ret;
        }
    }
}