﻿namespace TeleAnalyzer {
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for EpisodeTab.xaml
    /// </summary>
    public partial class EpisodeTab : UserControl {
        bool loading = true;

        public EpisodeTab() {
            this.InitializeComponent();
        }

        public bool Loading {
            get => this.loading;
            set {
                this.loading = value;
                this.Blocker.Opacity = value ? .6d : 0d;
            }
        }
    }
}