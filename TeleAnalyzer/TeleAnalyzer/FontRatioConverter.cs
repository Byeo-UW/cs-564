﻿namespace TeleAnalyzer {
    using System;
    using System.Globalization;
    using System.Windows.Data;

    internal class FontRatioConverter : IMultiValueConverter {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
            var first = (double) values[0] * (18 / 450d);
            var second = (double) values[1] * (18 / 800d);
            return first < second ? first : second;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) {
            return null;
        }
    }
}