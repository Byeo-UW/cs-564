﻿namespace TeleAnalyzer {
    using System.Collections.Generic;
    using System.Windows;
    using TeleAnalyzerAPI;

    /// <summary>
    ///     Interaction logic for ConglomeratePage.xaml
    /// </summary>
    public partial class ConglomeratePage : Window {
        List<string[]> conglomerates;
        MainWindow mw;
        public ConglomeratePage() {
            this.InitializeComponent();
            this.conglomerates = App.DbC.GetConglomerates();
            foreach (string[] tuple in this.conglomerates) {
                this.ComboBox.Items.Add(tuple[0]);
            }
        }

        void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e) {
            if (this.ComboBox.SelectedIndex < 0) return;
            mw = new MainWindow(this, this.conglomerates[this.ComboBox.SelectedIndex][0], this.conglomerates[this.ComboBox.SelectedIndex][1]);
            this.mw.Show();
            this.Close();
        }
    }
}