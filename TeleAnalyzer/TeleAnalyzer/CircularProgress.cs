﻿namespace TeleAnalyzer {
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Shapes;

    public class CircularProgress : Shape {
        // DependencyProperty - Value (0 - 100)
        static readonly FrameworkPropertyMetadata ValueMetadata =
            new FrameworkPropertyMetadata(
                0.0, // Default value
                FrameworkPropertyMetadataOptions.AffectsRender,
                null, // Property changed callback
                CoerceValue); // Coerce value callback

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(CircularProgress), ValueMetadata);

        static CircularProgress() {
            Brush b = new SolidColorBrush(Color.FromArgb(255, 252, 186, 3));
            b.Freeze();

            StrokeProperty.OverrideMetadata(
                typeof(CircularProgress),
                new FrameworkPropertyMetadata(b));
            FillProperty.OverrideMetadata(
                typeof(CircularProgress),
                new FrameworkPropertyMetadata(Brushes.Transparent));

            StrokeThicknessProperty.OverrideMetadata(
                typeof(CircularProgress),
                new FrameworkPropertyMetadata(10.0));
        }

        // Value (0-100)
        public double Value {
            get => (double) this.GetValue(ValueProperty);
            set => this.SetValue(ValueProperty, value);
        }

        protected override Size MeasureOverride(Size constraint) {
            // we will size ourselves to fit the available space
            return constraint;
        }

        protected override Geometry DefiningGeometry {
            get {
                const double startAngle = 90.0;
                var endAngle = 90.0 - this.Value / 100.0 * 360.0;

                var maxWidth = Math.Max(0.0, this.ActualWidth - this.StrokeThickness);
                var maxHeight = Math.Max(0.0, this.ActualHeight - this.StrokeThickness);

                var xStart = maxWidth / 2.0 * Math.Cos(startAngle * Math.PI / 180.0);
                var yStart = maxHeight / 2.0 * Math.Sin(startAngle * Math.PI / 180.0);

                var xEnd = maxWidth / 2.0 * Math.Cos(endAngle * Math.PI / 180.0);
                var yEnd = maxHeight / 2.0 * Math.Sin(endAngle * Math.PI / 180.0);

                var geom = new StreamGeometry();
                using (var ctx = geom.Open()) {
                    ctx.BeginFigure(
                        new Point(this.ActualWidth / 2.0 + xStart,
                            this.ActualHeight / 2.0 - yStart),
                        true, // Filled
                        false); // Closed
                    ctx.ArcTo(
                        new Point(this.ActualWidth / 2.0 + xEnd,
                            this.ActualHeight / 2.0 - yEnd),
                        new Size(maxWidth / 2.0, maxHeight / 2),
                        0.0, // rotationAngle
                        startAngle - endAngle > 180, // greater than 180 deg?
                        SweepDirection.Clockwise,
                        true, // isStroked
                        false);
                    //    ctx.LineTo(new Point((RenderSize.Width / 2.0), (RenderSize.Height / 2.0)), true, true);
                }

                return geom;
            }
        }

        static object CoerceValue(DependencyObject depObj, object baseVal) {
            var val = (double) baseVal;
            val = Math.Min(val, 99.999);
            val = Math.Max(val, 0.0);
            return val;
        }
    }
}