﻿namespace TeleAnalyzer {
    using System;
    using System.Globalization;
    using System.Windows.Data;

    internal class ColumnSplitter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (double) value / 3d;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            return (double) value * 3d;
        }
    }
}