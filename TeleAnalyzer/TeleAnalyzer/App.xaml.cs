﻿namespace TeleAnalyzer {
    using System.Windows;
    using TeleAnalyzerAPI;

    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        public static DbConnector DbC;

        public App() {
            DbC = new DbConnector();
            DbC.Start();
        }

        void App_OnExit(object sender, ExitEventArgs e) {
            DbC.Stop();
        }
    }
}