﻿namespace TeleAnalyzerAPI {
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using MySql.Data.MySqlClient;
    using Org.BouncyCastle.Asn1;
    using Org.BouncyCastle.Asn1.Eac;

    public class DbConnector {
        DbConnection dbCon;

        public void Start() {
            this.dbCon = DbConnection.Instance();
            this.dbCon.Server = "ratings-1.cajabhkwrq4b.us-east-2.rds.amazonaws.com";
            this.dbCon.DatabaseName = "ratings";
            this.dbCon.Password = "7mSrvee61vtAvxY5kthG";
        }

        public void Stop() {
            //Make sure to close connection to prevent
            //temporary network floating pointer
            this.dbCon.Close();
        }


        public int GetProgramIdFromTitle(string s) {
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT idProgram FROM program WHERE Title LIKE (@val1);";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@val1", s);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (int.TryParse(reader.GetString(0), out int result)) {
                    reader.Close();
                    return result;
                }
                Console.WriteLine("Attempted conversion failed()");
                reader.Close();
                return -2;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return -1;
        }


        public List<string[]> GetPrograms(string NetId) {
            var result = new List<string[]>();
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT DISTINCT Title, episode.Program_idProgram FROM episode, program WHERE episode.Network_idNetwork = @id AND episode.Program_idProgram = program.idProgram;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@id", NetId);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                while (reader.Read()) result.Add(new[] {reader.GetString(0), reader.GetString(1)});
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return null;
        }


        public List<int[]> GetSeasonAndEpisode(string PId, string NId) {
            var result = new List<int[]>();
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT DISTINCT Season, Episode_Number, idEpisode FROM episode WHERE Program_idProgram = @pid AND episode.Network_idNetwork = @nid ORDER BY Season ASC, Episode_Number ASC;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@pid", PId);
                cmd.Parameters.AddWithValue("@nid", NId);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                while (reader.Read()) result.Add(new[] {reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2)});
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return null;
        }


        public string[] GetNetworkData(string id) {
            string[] result = null;
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT Name, Type FROM network WHERE idNetwork = @id;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (reader.Read()) result = new[] {reader.GetString(0), reader.GetString(1)};
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }


        public List<string[]> GetNetworksPrograms(int id) {
            var result = new List<string[]>();
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT DISTINCT idProgram, Title from program p, episode e, network n WHERE p.idProgram = e.Program_idProgram AND e.Network_idNetwork = @id;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                while (reader.Read()) result.Add(new[] {reader.GetString(0), reader.GetString(1)});
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        public List<float[]> RatingsHistogram(string[] EIDs) {
            var result = new List<float[]>();
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT ROUND(A18_49_Rating, -.1) AS bucket, COUNT(*) AS COUNT, RPAD('', LN(COUNT(*)), '*') AS bar FROM ratings, episode WHERE episode.idEpisode IN (eids) AND episode.Ratings_idRatings = ratings.idRatings GROUP BY bucket ASC;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                //cmd.Parameters.AddWithValue("@pid", PId);
                var parameters = AddArrayParameters(cmd, EIDs, "eids");
                cmd.CommandText = $"SELECT ROUND(A18_49_Rating, -.1) AS bucket, COUNT(*) AS COUNT, RPAD('', LN(COUNT(*)), '*') AS bar FROM ratings, episode WHERE episode.idEpisode IN ({parameters}) AND episode.Ratings_idRatings = ratings.idRatings GROUP BY bucket ASC;";
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                while (reader.Read()) result.Add(new[] { reader.GetFloat(0), reader.GetFloat(1) });
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        public float Mean(string[] ids) {
            var result = float.NaN;
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT AVG(Total_Viewers) FROM ratings, episode WHERE episode.idEpisode IN (ids) AND episode.Ratings_idRatings = ratings.idRatings;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                var parameters = AddArrayParameters(cmd, ids, "ids");
                cmd.CommandText = $"SELECT AVG(Total_Viewers), AVG(A18_49_Rating), AVG(A18_49_Share) FROM ratings, episode WHERE episode.idEpisode IN ({parameters}) AND episode.Ratings_idRatings = ratings.idRatings;";
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (reader.Read()) result = reader.GetFloat(0);
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        public float StdDev(string[] ids) {
            var result = float.NaN;
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT STDDEV(Total_Viewers), STDDEV(A18_49_Rating), STDDEV(A18_49_Share) FROM ratings, episode WHERE episode.idEpisode IN (ids) AND episode.Ratings_idRatings = ratings.idRatings;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                var parameters = AddArrayParameters(cmd, ids, "ids");
                cmd.CommandText = $"SELECT STDDEV(Total_Viewers), STDDEV(A18_49_Rating), STDDEV(A18_49_Share) FROM ratings, episode WHERE episode.idEpisode IN ({parameters}) AND episode.Ratings_idRatings = ratings.idRatings;";
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (reader.Read()) result = reader.GetFloat(0);
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        public float NetworkRatingAverage(string NetId) {
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT AVG(A18_49_Rating) FROM ratings, episode WHERE episode.Network_idNetwork = @id AND episode.Ratings_idRatings = ratings.idRatings;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@id", NetId);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (reader.Read() && float.TryParse(reader.GetString(0), out float result)) {
                    reader.Close();
                    return result;
                }
                reader.Close();
                return float.NaN;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return float.NaN;
        }

        public int PortfolioCount(string NetId) {
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT count(DISTINCT program.idProgram) FROM program, network, episode WHERE episode.Network_idNetwork = @id AND episode.Program_idProgram = program.idProgram;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@id", NetId);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (reader.Read() && int.TryParse(reader.GetString(0), out int result)) {
                    reader.Close();
                    return result;
                }
                reader.Close();
                return -2;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return -1;
        }


        public List<string[]> NetworksByConglomerate(string id) {
            var result = new List<string[]>();
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT Name, idNetwork FROM network WHERE Conglomerate_id = @id ORDER BY Name ASC;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                while (reader.Read()) result.Add(new[] {reader.GetString(0), reader.GetString(1)});
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        public string[] GetEpisodeData(string SId, string EId, string PId) {
            string[] result = null;
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT Episode_Name, Date, Duration FROM episode WHERE Season = @sid AND Episode_Number = @eid AND Program_idProgram = @pid;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@sid", SId);
                cmd.Parameters.AddWithValue("@eid", EId);
                cmd.Parameters.AddWithValue("@pid", PId);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (reader.Read()) result = new[] {(reader.IsDBNull(0)) ? string.Empty : reader.GetString(0), reader.GetString(1), reader.GetString(2)};
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        public string[] GetRatingsData(string SId, string EId, string PId) {
            string[] result = null;
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT Total_Viewers, A18_49_Rating FROM ratings, episode WHERE Season = @sid AND Episode_Number = @eid AND Program_idProgram = @pid AND episode.Ratings_idRatings = ratings.idRatings;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                cmd.Parameters.AddWithValue("@sid", SId);
                cmd.Parameters.AddWithValue("@eid", EId);
                cmd.Parameters.AddWithValue("@pid", PId);
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                if (reader.Read()) result = new[] {reader.GetString(0), reader.GetString(1)};
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        public List<string[]> GetConglomerates() {
            var result = new List<string[]>();
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT Name,idtConglomerate FROM conglomerate ORDER BY Name ASC;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while(reader.Read()) result.Add(new [] {reader.GetString(0), reader.GetString(1)});
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        public List<float> RatingsGraph(string[] EIDs) {
            var result = new List<float>();
            if (this.dbCon.IsConnect()) {
                const string query = "SELECT Total_Viewers FROM ratings, episode WHERE episode.idEpisode IN (eids) AND episode.Ratings_idRatings = ratings.idRatings ORDER BY Season ASC, Episode_Number ASC;";
                var cmd = new MySqlCommand(query, this.dbCon.Connection);
                //cmd.Parameters.AddWithValue("@pid", PId);
                var parameters = AddArrayParameters(cmd, EIDs, "eids");
                cmd.CommandText = $"SELECT Total_Viewers FROM ratings, episode WHERE episode.idEpisode IN ({parameters}) AND episode.Ratings_idRatings = ratings.idRatings ORDER BY Season ASC, Episode_Number ASC;";
                cmd.Prepare();
                var reader = cmd.ExecuteReader();
                while (reader.Read()) result.Add(reader.GetFloat(0));
                reader.Close();
                return result;
            }
            Debug.WriteLine("Attempted Connection on non-connected database");
            this.Start();
            return result;
        }

        static string AddArrayParameters(MySqlCommand sqlCommand, string[] array, string paramName) {
            /* An array cannot be simply added as a parameter to a SqlCommand so we need to loop through things and add it manually. 
             * Each item in the array will end up being it's own SqlParameter so the return value for this must be used as part of the
             * IN statement in the CommandText.
             */
            var parameters = new string[array.Length];
            for (var i = 0; i < array.Length; i++) {
                parameters[i] = $"@{paramName}{i}";
                sqlCommand.Parameters.AddWithValue(parameters[i], array[i]);
            }

            return string.Join(", ", parameters);
        }
    }
}

//public void SampleQuery() {
//    if (this.dbCon.IsConnect()) {
//        //suppose col0 and col1 are defined as VARCHAR in the DB
//        var param = "Modern Family";
//        var query = "SELECT Season, Episode_Number FROM episode WHERE Program_idProgram IN (SELECT idProgram FROM program WHERE Title LIKE @val1) ORDER BY season, Episode_Number; ";
//        var cmd = new MySqlCommand(query, this.dbCon.Connection);
//        cmd.Parameters.AddWithValue("@val1", param);
//        cmd.Prepare();
//        var reader = cmd.ExecuteReader();
//        while (reader.Read()) {
//            //var stringFromProperty = reader.GetString("key_name");
//            var someStringFromColumnZero = reader.GetString(0);
//            var someStringFromColumnOne = reader.GetString(1);
//            Console.WriteLine(someStringFromColumnZero + "," + someStringFromColumnOne);
//        }
//    } else {
//        Debug.WriteLine("Attempted Connection on non-connected database");
//        this.Start();
//    }
//}