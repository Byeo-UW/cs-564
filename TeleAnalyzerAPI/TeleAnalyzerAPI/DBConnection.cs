﻿namespace TeleAnalyzerAPI {
    using MySql.Data.MySqlClient;

    public class DbConnection {
        static DbConnection _instance;
        
        public string DatabaseName { get; set; } = string.Empty;
        public string Password { get; set; }
        public string Server { get; set; }
        public MySqlConnection Connection { get; set; }

        public static DbConnection Instance() {
            return _instance ?? (_instance = new DbConnection());
        }

        public bool IsConnect() {
            if (this.Connection != null) return true;

            if (string.IsNullOrEmpty(this.DatabaseName)) return false;
            this.Connection = new MySqlConnection($"Server={this.Server}; database={this.DatabaseName}; UID=admin; password={this.Password}; pooling=true; Allow User Variables=True");
            this.Connection.Open();
            return true;
        }

        public void Close() {
            this.Connection?.Close();
        }
    }
}