﻿namespace QueryTesting {
    using System;
    using System.Collections.Generic;
    using TeleAnalyzerAPI;

    internal static class Program {
        static void Main(string[] args) {
            var dbC = new DbConnector();
            dbC.Start();
            //List<int[]> result = dbC.GetEpisodeNumbers(20);
            //foreach (int[] item in result) {
            //    Console.WriteLine(string.Join(",", item));
            //}
            //List<string[]> result = dbC.GetConglomerates();
            //foreach (string[] item in result) {
            //    Console.WriteLine(string.Join(",", item));
            //}
            //Console.WriteLine(dbC.NetworkRatingAverage("1"));
            //Console.WriteLine(dbC.PortfolioCount("1"));
            //List<string[]> result = dbC.GetPrograms("1");
            //foreach (string[] item in result) {
            //    Console.WriteLine(string.Join(",", item));
            //}
            //List<int[]> result = dbC.GetSeasonAndEpisode("1");
            //foreach (int[] item in result) {
            //    Console.WriteLine(string.Join(",", item));
            //}
            //Console.WriteLine(string.Join(",", dbC.GetEpisodeData("1","1","1")));
            //Console.WriteLine(string.Join(",", dbC.GetRatingsData("1","1","1")));
            List<float[]> result = dbC.RatingsHistogram(new []{"1","2","3"}, "38");
            foreach (float[] item in result) {
                Console.WriteLine(string.Join(",", item));
            }

            dbC.Stop();
            Console.ReadKey();
        }
    }
}